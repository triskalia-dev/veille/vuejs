/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */

//TODO 
import DragDrop from '../src/views/DragDrop.vue'

const wrapper = shallowMount(Hello)

describe('TestDragDrop', () => {
  it('returns false if <0', () => {
    expect(DragDrop.testfunction(-3)).toBe(false)
  })
  it('returns true if >0', () => {
    expect(DragDrop.testfunction(3)).toBe(true)
  })
})
