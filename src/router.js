import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import DragDrop from './views/DragDrop.vue'
import ServerSideTable from './views/ServerSideTable.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/dd',
      name: 'dragdrop',
      component: DragDrop
    },
    {
      path: '/serversidetable',
      name: 'table serverside',
      component: ServerSideTable
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
