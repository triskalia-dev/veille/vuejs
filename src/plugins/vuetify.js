import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  iconfont: 'fa',
  theme: {
    primary: '#EE285F', // #E53935
    secondary: '#FFFFFF', // #FFCDD2
    accent: '#FFEEDD' // #3F51B5
  }
})
