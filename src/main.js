import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import store from './store'
import '@fortawesome/fontawesome-free/css/all.css'

import _ from 'underscore'

Vue.config.productionTip = false

Vue.use(_)

const vm = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
